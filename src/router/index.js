import Vue from 'vue'
import Router from 'vue-router'
import Customers from '@/views/Customers.vue'
import Products from '@/views/Products.vue'

Vue.use(Router)

 const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/customers',
      name: 'customers',
      component: Customers,
    },
    {
      path: '/products',
      name: 'products',
      component: Products,
    },
  ],
})

export default router