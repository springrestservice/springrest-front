import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    gridTitle: [],
    gridData: [],
  },
  mutations: {
    SET_GRID_DATA: (state, data) => {
      if (data.length > 0) {
        state.gridTitle = []
        for (var key of Object.keys(data[0])) {
          state.gridTitle.push({
            text: key,
            value: key,
          })
        }
      }
      state.gridData = data
    },
  },
  actions: {
    GET_GRID_DATA ({ commit }, url) {
      return axios(url, {
        method: 'GET'
      })
      .then((result) => {
        commit('SET_GRID_DATA', result.data)
      })
    },
  },
  getters: {
    DATAGRIDTITLE (state) {
      return state.gridTitle
    },
    DATAGRIDCONTENT (state) {
      return state.gridData
    }
  },

})
